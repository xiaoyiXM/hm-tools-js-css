package top.hmtools.jsCss.common;

import java.io.Serializable;

/**
 * 静态资源（具体内容为文本）描述实体类
 * @author Jianghaibo
 *
 */
public class ResourcesBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 5449962550771368129L;

    /**
     * 基本文件名
     * <br>文件名：aaa.txt  ==》 fileBaseName：aaa
     * <br>文件名：aaa.txt.jpg  ==》 fileBaseName：aaa.txt
     */
    private String fileBaseName;
    
    /**
     * 文件扩展名
     * <br>文件名：aaa.txt  ==》 fileExtension：txt
     * <br>文件名：aaa.txt.jpg  ==》 fileExtension：jpg
     */
    private String fileExtension;
    
    /**
     * 文件全名称（ 基本文件名.文件扩展名）
     * <br>文件名：aaa.txt 
     * <br>文件名：aaa.txt.jpg
     */
    private String fileFullName;
    
    /**
     * 绝对路径（含文件名）
     */
    private String fileAbsolutelyPath;
    
    /**
     * 绝对路径（不含文件名）
     */
    private String fileAbsolutelyDir;
    
    /**
     * 相对路径（含文件名）
     */
    private String fileRelativePath;
    
    /**
     * 相对路径（不含文件名）
     */
    private String fileRelativeDir;
    
    /**
     * 文件具体文本内容
     */
    private String fileContent;
    
    /**
     * 原始字符编码
     */
    private String encoding;
    
    /**
     * 索引键值
     */
    private String keyName;
    
    /**
     * 获取当前文件所在的根路径（即配置监控的路径）
     */
    private String rootPath;

    public String getFileBaseName() {
        return fileBaseName;
    }

    public void setFileBaseName(String fileBaseName) {
        this.fileBaseName = fileBaseName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getFileFullName() {
        return fileFullName;
    }

    public void setFileFullName(String fileFullName) {
        this.fileFullName = fileFullName;
    }

    public String getFileAbsolutelyPath() {
        return fileAbsolutelyPath;
    }

    public void setFileAbsolutelyPath(String fileAbsolutelyPath) {
        this.fileAbsolutelyPath = fileAbsolutelyPath;
    }

    public String getFileRelativePath() {
        return fileRelativePath;
    }

    public void setFileRelativePath(String fileRelativePath) {
        this.fileRelativePath = fileRelativePath;
        if(this.fileRelativePath != null && this.fileRelativePath.length()>0){
        	this.keyName = this.fileRelativePath;
        	//统一分隔符为“/”
        	this.keyName = this.keyName.replaceAll("\\\\", "/");
        	//去掉头部的单个或者多个“/”
        	this.keyName = this.keyName.replaceAll("^/*", "");
        	//替换“/”为“_”
        	this.keyName = this.keyName.replaceAll("/+", "_");
        }
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getFileAbsolutelyDir() {
        return fileAbsolutelyDir;
    }

    public void setFileAbsolutelyDir(String fileAbsolutelyDir) {
        this.fileAbsolutelyDir = fileAbsolutelyDir;
    }

    public String getFileRelativeDir() {
        return fileRelativeDir;
    }

    public void setFileRelativeDir(String fileRelativeDir) {
        this.fileRelativeDir = fileRelativeDir;
    }

	public String getKeyName() {
		return keyName;
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
    
    
}
