package top.hmtools.jsCss;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import top.hmtools.jsCss.autoConfiguration.JsCssAutoConfiguration;

/**
 * 启用JsCss内容合并组件
 * @author Jianghaibo
 *
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = { java.lang.annotation.ElementType.TYPE })
@Documented
@Import({JsCssAutoConfiguration.class})
public @interface EnableJsCss {

}
